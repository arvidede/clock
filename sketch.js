var time;
function setup() {
    createCanvas(400, 400)
    angleMode(DEGREES)
    var params = getURLParams()
    time = params.time
}

function draw() {
    background(255)
    translate(width / 2, height / 2)
    rotate(-90)

    let end = map(millis()/1000, 0, time, 0, 360)
    fill(120)
    arc(0,0, 300, 300, 0, 360 - end)

    strokeWeight(5)
    stroke(100)
    noFill()
    ellipse(0,0, 300, 300)

    strokeWeight(1)
    fill(255)
}
